import 'package:flutter/material.dart';


class Inscription extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        title: const Text("INSCRIVEZ-VOUS"),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin:EdgeInsets.symmetric(
            vertical: 10,
            horizontal: 30,

          ),
          child: Column(
            children : [
              Container(
                height: 200,
                child: Image.asset("assets/images/minecraft.jpg",width: 2000,),

              ),

              Container(
                child: TextField(
                  decoration: InputDecoration(
                    labelText:'username',
                    labelStyle: TextStyle(
                      color: Colors.grey[400],
                    )
                  ),
                ),

              ),
              Container(
                child: TextField(
                  decoration: InputDecoration(
                      labelText:'Email',
                      labelStyle: TextStyle(
                        color: Colors.grey[400],
                      )
                  ),
                ),

              ),
              Container(
                child: TextField(
                  obscureText: false,

                  decoration: InputDecoration(
                    labelStyle: TextStyle(
                      color: Colors.grey[400],
                    ),
                      labelText:'password',
                      suffixIcon: IconButton(
                        icon: Icon(
                        Icons.visibility,
                        color: Colors.black,
                        ),
                        onPressed: (){

                        },
                      ),

                  ),
                ),

              ),
              Container(
                child: TextField(
                  decoration: InputDecoration(
                      labelText:'Année de naissance',
                      labelStyle: TextStyle(
                        color: Colors.grey[400],
                      )
                  ),
                ),

              ),

         
              Container(
                child: TextField(
                  decoration: InputDecoration(
                      labelText:'Adresse de Facturation',
                      labelStyle: TextStyle(
                        color: Colors.grey[400],
                      )
                  ),
                ),

              ),

              Container(
                margin: EdgeInsets.only(
                  top: 30,
                  bottom: 20,
                ),

                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      padding: EdgeInsets.all(9)
                  ),
                  onPressed: () {},

                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.add_rounded),
                      SizedBox(width:10),
                      Text("inscription")
                    ],
                  ),

                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 30,
                  bottom: 20,
                ),

                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      padding: EdgeInsets.all(9)
                  ),
                  onPressed: () {},

                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.arrow_back),
                      SizedBox(width:10),
                      Text("Annuler")
                    ],
                  ),

                ),
              )

            ],
          ),
        ),

      ),

    );



  }
}
