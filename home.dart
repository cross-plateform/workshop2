
import 'package:flutter/material.dart';

import 'product_informations.dart';




class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("G-Store ESPRIT"),
      ),
      body: Column(
          children: [

            ProductInfos("assets/images/dmc5.jpg","Devil May Cry",200),
            ProductInfos("assets/images/re8.jpg","Resident Evil VIII",200),
            ProductInfos("assets/images/nfs.jpg","Need For Speed Heat",100),
            ProductInfos("assets/images/rdr2.jpg","Red Dead Redenption II",150),
            ProductInfos("assets/images/fifa.jpg","FIFA 2",100),
          ],


      ),
    );
  }
  }


