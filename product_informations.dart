import 'package:flutter/material.dart';

class ProductInfos extends StatelessWidget {
 final String _title;
 final String _image;
 final int _price;

  ProductInfos( this._image,this._title,this._price);



  @override
  Widget build(BuildContext context) {
return Card(
child: Row(
crossAxisAlignment: CrossAxisAlignment.center,
children: [

Container(
margin:const  EdgeInsets.fromLTRB(10, 10, 20, 10),
child: Image.asset(_image,width: 200,height: 94)

),
Column(
 crossAxisAlignment: CrossAxisAlignment.start,
 children: [
Text(_title),
  Text(_price.toString() + "TND",textScaleFactor: 2,)
],
)


 ],

 ),

);
}
  }



