import 'package:flutter/material.dart';
import 'home.dart';
import 'package:workshop_s2/details.dart';
import 'package:workshop_s2/inscription.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      title: "My First Flutter App",
      home: Inscription(),
    );
  }
}